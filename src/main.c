#include "mem.h"
#include "mem_internals.h"

#define assert(condition, test_number) if (!(condition)) { \
         printf("Test \"%d\"\n", test_number);     \
         heap_term();\
         return;\
}

static void test_success_alloc() {
    heap_init(1);
    void* mem = _malloc(300);
    struct block_header* header = block_get_header(mem);

    assert(header->capacity.bytes == 300, 1);
    _free(mem);
    heap_term();
    printf("Test 1 passed!\n");
}

static void test_free_one_block() {

    heap_init(1);
    void* mem = _malloc(150);
    void* mem1 = _malloc(250);
    void* mem2 = _malloc(350);
    _free(mem);

    assert(block_get_header(mem)->is_free, 2)
    assert(!block_get_header(mem1)->is_free, 2)
    assert(!block_get_header(mem2)->is_free, 2)
    _free(mem2);
    _free(mem1);
    heap_term();

    printf("Test 2 passed!\n");
}

static void test_free_three_blocks() {
    heap_init(1);
    void* mem = _malloc(150);
    block_capacity capacity = block_get_header(mem)->next->capacity;
    void* mem1 = _malloc(250);
    void* mem2 = _malloc(300);

    _free(mem2);
    _free(mem1);

    assert(block_get_header(mem1)->capacity.bytes == capacity.bytes, 3)
    heap_term();

    printf("Test 3 passed!\n");
}

static void test_alloc_new_region() {
    heap_init(1);
    void* mem = _malloc(10000);
    struct block_header* header = block_get_header(mem);
    assert(header->capacity.bytes == 10000 && header->next->capacity.bytes > REGION_MIN_SIZE, 4)

    printf("Test 4 passed!\n");
}

static void test_alloc_new_region_mmap() {
    void* odd_alloc = mmap(HEAP_START + REGION_MIN_SIZE, 200, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
    heap_init(1);
    uint8_t* mem = _malloc(10000);
    struct block_header* header = block_get_header(mem);
    assert(header != odd_alloc && header->capacity.bytes == 9000, 5);

    munmap(odd_alloc, 200);
    heap_term();
    printf("Test 5 passed!\n");
}

int main() {
    test_success_alloc();
    test_free_one_block();
    test_free_three_blocks();
    test_alloc_new_region();
    test_alloc_new_region_mmap();

    return 0;
}
